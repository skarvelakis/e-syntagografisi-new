package utilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertiesManager {
    Properties props = null;

    public PropertiesManager(String file) {
        readProperties(file);
    }

    private void readProperties(String file) {
        InputStream is = null;
        this.props = new Properties();
        try {
            Path path = Paths.get("../conf/".concat(file));
            if (Files.exists(path)) {
                is = new FileInputStream(path.getFileName().toFile());
            } else {
                is = this.getClass().getClassLoader().getResourceAsStream(file);
            }
            this.props = new Properties();
            this.props.load(is);
            is.close();
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
        } finally {
            if (is != null)
                try {
                    is.close();
                } catch (IOException e) {
                    System.err.println(e.fillInStackTrace());
                }
        }
    }

    public String getProperty(String key) {
        return this.props.getProperty(key);
    }

    public void setProperty(String key, String value) {
        this.props.setProperty(key, value);
    }

    public Properties getProperties() {
        return this.props;
    }
}