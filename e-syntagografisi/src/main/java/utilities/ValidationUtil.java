package utilities;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

public final class ValidationUtil {
	private static final Pattern INTEGER_PATTERN = Pattern.compile("^[0-9]*$");

	private static final Pattern NUMERIC_PATTERN = Pattern
			.compile("^\\-?\\d+\\.?\\d*$");

	public static boolean fieldPresent(Object o) {
		if (o == null)
			return false;
		if ((o instanceof String))
			return ((String) o).trim().length() != 0;
		if ((o instanceof StringBuilder))
			return ((StringBuilder) o).length() != 0;
		if ((o instanceof Collection))
			return !((Collection<?>) o).isEmpty();
		if ((o instanceof Map))
			return !((Map<?, ?>) o).isEmpty();
		if ((o instanceof Object[]))
			return ((Object[]) (Object[]) o).length != 0;
		return true;
	}

	public static boolean fieldPresentNotNull(Object o) {
		if ((o instanceof Collection)) {
			((Collection<?>) o).removeAll(Collections.singleton(null));
		}
		return fieldPresent(o);
	}

	public static boolean fieldNotPresentOrNull(Object o) {
		return !fieldPresentNotNull(o);
	}

	public static boolean allFieldsPresent(Object[] objList) {
		boolean isPresent = true;
		for (Object obj : objList) {
			if (!fieldPresent(obj)) {
				isPresent = false;
				break;
			}
		}
		return isPresent;
	}

	public static boolean fieldNotPresent(Object o) {
		return !fieldPresent(o);
	}

	public static final boolean isNumeric(String s) {
		return fieldPresent(s) ? NUMERIC_PATTERN.matcher(s).matches() : false;
	}

	public static final boolean isInteger(String s) {
		return fieldPresent(s) ? INTEGER_PATTERN.matcher(s).matches() : false;
	}

	public static final boolean isNumeric(String s, boolean decimalPart) {
		return decimalPart ? isNumeric(s) : isInteger(s);
	}

	public static boolean isTrue(Object o) {
		if (o == null)
			return false;
		if ((o instanceof String))
			return Boolean.TRUE.toString()
					.equalsIgnoreCase(o.toString().trim());
		if ((o instanceof Boolean)) {
			return ((Boolean) o).booleanValue();
		}
		return false;
	}

	public static boolean contains(String val, CharSequence s) {
		return (fieldPresent(val)) && (val.contains(s));
	}
}