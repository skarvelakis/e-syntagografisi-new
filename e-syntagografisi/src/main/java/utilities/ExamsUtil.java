package utilities;

import internal.Examinations;

import java.io.*;

public class ExamsUtil {
	public static String DATA_XML = "examinations.xml";

	public static Examinations getExamsAsObject() {
		String fileData  = readFileAsString(DATA_XML);
		Examinations exams = PrintUtil.parseMessage(fileData, Examinations.class);
		return exams;
	}

	private static String readFileAsString(String fileName) {
		try {
			InputStream stream = ExamsUtil.class.getClassLoader().getResourceAsStream(fileName);
			Reader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
			StringBuilder builder = new StringBuilder();
			char[] buffer = new char[8192];
			int read;
			while ((read = reader.read(buffer, 0, buffer.length)) > 0) {
				builder.append(buffer, 0, read);
			}
			stream.close();
			return builder.toString();
		} catch (IOException e) {
		}
		return null;
	}
}
