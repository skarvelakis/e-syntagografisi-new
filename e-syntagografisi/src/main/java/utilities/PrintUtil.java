package utilities;

import java.io.ByteArrayInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class PrintUtil {
	private static Logger logger = Logger.getLogger(PrintUtil.class.getName());
	private static final StringWriter stringWriter = new StringWriter();
	private static final PrintWriter printWriter = new PrintWriter(stringWriter);
	private static final List<String> PACKAGE_NAMES_LIST = Arrays.asList("internal" );
	public static final String ENCODING = "UTF-8";
	private static Map<String, Marshaller> MARSHALLER_MAP;
	private static Map<String, Unmarshaller> UNMARSHALLER_MAP;

	public static synchronized String formatMessage(Object message) {
		try {
			if (message != null) {
				Class<? extends Object> clazz = message.getClass();
				if ((clazz.getPackage() == null) || (clazz.getPackage().getName() == null) || (clazz.getPackage().getName().isEmpty())) {
					return "";
				}
				String packageName = clazz.getPackage().getName();

				Marshaller marshaller = getMarshaller(packageName);

				StringWriter sw = new StringWriter();
				marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
				marshaller.marshal(message, sw);
				return sw.toString();
			}
			return "";
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static synchronized <T> T parseMessage(String message, Class<T> className) {
		Object ret = null;
		Object obj = null;
		try {
			String packageName = className.getPackage().getName();
			logger.severe(new StringBuilder().append("packageName:").append(packageName).toString());
			ByteArrayInputStream bs = new ByteArrayInputStream(message.getBytes("UTF-8"));
			SAXSource saxSource = new SAXSource(new InputSource(bs));
			Unmarshaller unMarshaller = getUnMarshaller(packageName);
			obj = unMarshaller.unmarshal(saxSource);
			if ((obj instanceof JAXBElement)) {
				JAXBElement jaxbElement = (JAXBElement) JAXBElement.class.cast(obj);
				ret = jaxbElement.getValue();
			} else {
				ret = className.cast(obj);
			}
		} catch (Exception m) {
			logger.severe(formatThrowableStackTrace(m));
		}
		return (T) ret;
	}

	public static synchronized Object parseMessageWithException(String message, Class<?> className) throws UnmarshalException {
		Object obj = null;
		try {
			String packageName = className.getPackage().getName();
			logger.severe(new StringBuilder().append("packageName:").append(packageName).toString());
			SAXParserFactory spFactory = SAXParserFactory.newInstance();
			spFactory.setNamespaceAware(true);
			ByteArrayInputStream bs = new ByteArrayInputStream(message.getBytes("UTF-8"));
			SAXSource saxSource = new SAXSource(new InputSource(bs));

			Unmarshaller unMarshaller = getUnMarshaller(packageName);
			obj = unMarshaller.unmarshal(saxSource);
		} catch (UnmarshalException um) {
			logger.severe(formatThrowableStackTrace(um));
			throw um;
		} catch (JAXBException e) {
			logger.severe(formatThrowableStackTrace(e));
		} catch (UnsupportedEncodingException e) {
			logger.severe(formatThrowableStackTrace(e));
		}
		return obj;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void initializeMarshallerMap() {
		MARSHALLER_MAP = new HashMap();
		Marshaller marshaller = null;
		for (String packageName : PACKAGE_NAMES_LIST)
			try {
				marshaller = JAXBContext.newInstance(packageName).createMarshaller();
				marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
				marshaller.setProperty("jaxb.encoding", "UTF-8");
			} catch (PropertyException ex) {
			} catch (JAXBException e) {
				logger.severe(formatThrowableStackTrace(e));
			} finally {
				MARSHALLER_MAP.put(packageName, marshaller);
			}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void initializeUnMarshallerMap() {
		UNMARSHALLER_MAP = new HashMap();
		Unmarshaller unMarshaller = null;
		for (String packageName : PACKAGE_NAMES_LIST)
			try {
				unMarshaller = JAXBContext.newInstance(packageName).createUnmarshaller();
				unMarshaller.setProperty("jaxb.encoding", "UTF-8");
			} catch (PropertyException ex) {
			} catch (JAXBException e) {
				logger.severe(formatThrowableStackTrace(e));
			} finally {
				UNMARSHALLER_MAP.put(packageName, unMarshaller);
			}
	}

	public static Marshaller getMarshaller(String packageName) {
		if (ValidationUtil.fieldNotPresent(MARSHALLER_MAP)) {
			logger.severe("Initializing Marshaller..");
			initializeMarshallerMap();
		}
		Marshaller marshaller = (Marshaller) MARSHALLER_MAP.get(packageName);
		if (marshaller == null) {
			logger.warning(new StringBuilder().append("Print method does not support the provided context:").append(packageName).toString());
			try {
				marshaller = JAXBContext.newInstance(packageName).createMarshaller();
				marshaller.setProperty("jaxb.formatted.output", Boolean.TRUE);
				marshaller.setProperty("jaxb.encoding", "UTF-8");
			} catch (PropertyException ex) {
			} catch (JAXBException e) {
				logger.severe(formatThrowableStackTrace(e));
			} finally {
				MARSHALLER_MAP.put(packageName, marshaller);
			}
		}

		return marshaller;
	}

	public static Unmarshaller getUnMarshaller(String packageName) {
		if (ValidationUtil.fieldNotPresent(UNMARSHALLER_MAP)) {
			logger.severe("Initializing Unmarshaller..");
			initializeUnMarshallerMap();
		}
		Unmarshaller unMarshaller = (Unmarshaller) UNMARSHALLER_MAP.get(packageName);
		if (unMarshaller == null) {
			logger.warning(new StringBuilder().append("Print method does not support the provided context:").append(packageName).toString());
			try {
				unMarshaller = JAXBContext.newInstance(packageName).createUnmarshaller();
				unMarshaller.setProperty("jaxb.encoding", "UTF-8");
			} catch (PropertyException ex) {
			} catch (JAXBException e) {
				logger.severe(formatThrowableStackTrace(e));
			} finally {
				UNMARSHALLER_MAP.put(packageName, unMarshaller);
			}
		}

		return unMarshaller;
	}

	public static String formatDomDocument(Document document) {
		try {
			DOMSource domSource = new DOMSource(document);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty("method", "xml");
			transformer.setOutputProperty("encoding", "UTF-8");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "3");
			transformer.setOutputProperty("indent", "yes");
			StringWriter sw = new StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			String xml = sw.toString();
			return new StringBuilder().append("\n").append(xml).toString();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static String formatThrowableStackTrace(Throwable throwable) {
		StringWriter stringWriter = new StringWriter();
		throwable.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}

	public static String printLine(Object[] args) {
		StringBuilder builder = new StringBuilder();
		for (Object token : args) {
			builder.append(token);
		}
		builder.append("\n");

		return builder.toString();
	}

	public static String printField(String name, Object value) {
		String result = null;
		if (value == null)
			result = printLine(new Object[] { name, " is null" });
		else {
			result = printLine(new Object[] { name, ": \"", value.toString(), "\"", " (type is: ", value.getClass().getName(), ")" });
		}

		return result;
	}

	public static <T> String printList(List<T> argList) {
		StringBuilder builder = new StringBuilder();
		Iterator<?> i;
		if (ValidationUtil.fieldPresent(argList))
			for (i = argList.iterator(); i.hasNext();) {
				Object val = i.next();
				builder.append(printLine(new Object[] { "Value: ", val.toString() }));
			}
		else {
			builder.append("List cannot be printed. It is NULL!\n");
		}

		return builder.toString();
	}

	public static String printf(String format, Object[] args) {
		printWriter.printf(format, args);
		String s = stringWriter.toString();
		stringWriter.getBuffer().delete(0, s.length());
		return s;
	}
}
