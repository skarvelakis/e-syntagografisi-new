package flow;

import utilities.ExamsUtil;
import utilities.PrintUtil;
import utilities.ValidationUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EOPYYForm extends JFrame {
    private static final long serialVersionUID = -6878737724613369774L;

    private JPanel ivjJFrameContentPane = null;

    private JToolBar ivjJToolBar = null;

    private JButton ivjJButton1 = null;
    private JButton ivjJButton2 = null;
    private JButton ivjJButton3 = null;
    private JButton ivjJButton7 = null;
    private JButton ivjJButton8 = null;
    private JButton ivjJButton9 = null;
    private JButton ivjJButton10 = null;
    private JButton ivjJButton11 = null;
    private JButton ivjJButton12 = null;
    private JButton ivjJButton13 = null;

    private JPanel ivjJPanel = null;
    private JPanel ivjJPanel1 = null;

    private List<JCheckBox> jCheckBoxList = null;
    private List<JCheckBox> ailmentsList = null;

    private Syntagografisi syntagografisi;
    private DisplayDiseases displayDiseases;

    private JTextField amka;
    private JTextField reasonOfVisit;
    private JTextField id;

    private List<Integer> prescriptionDiagnosis;

    private static Logger logger = Logger.getLogger(EOPYYForm.class.getName());

    public EOPYYForm() {
        initialize();
    }

    public EOPYYForm(Syntagografisi syntagografisi, DisplayDiseases displayDiseases) {
        this.jCheckBoxList = new ArrayList();
        this.ailmentsList = new ArrayList();
        this.syntagografisi = syntagografisi;
        this.displayDiseases = displayDiseases;
        initialize();
    }

    private void initialize() {
        for (int i = 0; i < syntagografisi.getExaminations().getDiseases().size(); i++) {
            if (i + 1 >= 10) {
                JCheckBox jCheckBox = new JCheckBox("("
                        .concat(syntagografisi.getExaminations().getDiseases().get(i).getNumber())
                        .concat(").     ")
                        .concat(syntagografisi.getExaminations().getDiseases().get(i).getIllness()));

                jCheckBox.setPreferredSize(new Dimension(33, 28));
                jCheckBoxList.add(jCheckBox);
            } else {
                JCheckBox jCheckBox = new JCheckBox("("
                        .concat(syntagografisi.getExaminations().getDiseases().get(i).getNumber())
                        .concat(").      ")
                        .concat(syntagografisi.getExaminations().getDiseases().get(i).getIllness()));

                jCheckBox.setPreferredSize(new Dimension(33, 28));
                jCheckBoxList.add(jCheckBox);
            }
        }
        for (int i = 0; i < syntagografisi.getAilments().getAilments().size(); i++) {
            ailmentsList.add(new JCheckBox(syntagografisi.getAilments().getAilments().get(i).getDescription()));
        }
        logger.info("Initiate EOPYY application...");
        setContentPane(getJFrameContentPane());
        setName("JFrame1");
        setTitle("����������� �������������� V.0-15");
        setDefaultCloseOperation(3);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                try {
                    dispose();
                    syntagografisi.tearDown();
                } catch (Exception e) {
                    logger.severe(e.getMessage());
                }
            }
        });
        setSize(1200, 680);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int x = (screenSize.width - getWidth()) / 2;
        int y = (screenSize.height - getHeight()) / 2;
        setLocation(x, y);
        setVisible(true);
        setResizable(false);
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            SwingUtilities.updateComponentTreeUI(this);
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }
    }

    private JPanel getJFrameContentPane() {
        if (ivjJFrameContentPane == null) {
            ivjJFrameContentPane = new JPanel();
        }
        BorderLayout layBorderLayout_3 = new BorderLayout();
        ivjJFrameContentPane.setLayout(layBorderLayout_3);
        ivjJFrameContentPane.add(getIvjJPanel(), "Center");
        ivjJFrameContentPane.add(getIvjJToolBar(), "Last");
        ivjJFrameContentPane.setName("JFrameContentPane");
        return ivjJFrameContentPane;
    }

    private JPanel getIvjJToolBar() {
        if (ivjJPanel1 == null) {
            ivjJPanel1 = new JPanel();
        }
        if (ivjJToolBar == null) {
            ivjJToolBar = new JToolBar();
        }
        GridLayout experimentLayout = new GridLayout(3, 4);
        ivjJToolBar.setLayout(experimentLayout);
        ivjJToolBar.add(startApplication());
        ivjJToolBar.add(startVisit());
        ivjJToolBar.add(startExaminations());
        ivjJToolBar.add(searchPrescription());
        ivjJToolBar.add(insertPersonalDetails());
        ivjJToolBar.add(closeVisit());
        ivjJToolBar.add(startPresription());
        ivjJToolBar.add(searchExaminations());
        ivjJToolBar.add(new JLabel(""));
        ivjJToolBar.add(new JLabel(""));
        ivjJToolBar.add(startExaminationsWithoutDisease());
        ivjJToolBar.add(displayExams());
        ivjJPanel1.add(ivjJToolBar, "North");
        return ivjJPanel1;
    }

    private JButton startVisit() {
        if (ivjJButton2 == null) {
            ivjJButton2 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton2.getFont().getName(), ivjJButton2.getFont().getStyle(), 14);
        ivjJButton2.setFont(newButtonFont);
        ivjJButton2.setText("�������� ��������");
        ivjJButton2.addActionListener(ae -> {
            if (syntagografisi != null) {
                try {
                    final JFrame frame = new JFrame("��������");
                    frame.setTitle("�������� ��������");

                    JButton insert = new JButton("��������");
                    JButton bClose = new JButton("��������");

                    JPanel checkPanel = new JPanel(new GridLayout(2, 2));
                    JLabel label = new JLabel("����");
                    JLabel label1 = new JLabel("���� ���������");

                    Font newButtonFont1 = new Font(label.getFont().getName(), label.getFont().getStyle(), 14);
                    label.setFont(newButtonFont1);
                    label1.setFont(newButtonFont1);

                    amka = new JTextField(40);
                    reasonOfVisit = new JTextField(40);

                    checkPanel.add(label);
                    checkPanel.add(amka);
                    checkPanel.add(label1);
                    checkPanel.add(reasonOfVisit);

                    JPanel p1 = new JPanel();
                    p1.add(insert);
                    p1.add(bClose);

                    frame.getContentPane().add(p1, "Last");
                    frame.getContentPane().add(checkPanel, "Center");
                    frame.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent ev) {
                            frame.dispose();
                        }
                    });
                    frame.setSize(500, 200);
                    Toolkit toolkit = Toolkit.getDefaultToolkit();
                    Dimension screenSize = toolkit.getScreenSize();

                    int x = (screenSize.width - frame.getWidth()) / 2;
                    int y = (screenSize.height - frame.getHeight()) / 2;

                    frame.setLocation(x, y);
                    frame.setVisible(true);
                    frame.setResizable(false);
                    bClose.addActionListener(ae1 -> frame.dispose());

                    insert.addActionListener(ae1 -> {
                        SwingWorker<Void, Void> worker = new SwingWorker() {
                            protected Void doInBackground()
                                    throws Exception {
                                if (ValidationUtil.fieldPresent(amka.getText())) {
                                    syntagografisi.setAmka(amka.getText());
                                    if (ValidationUtil.fieldPresent(reasonOfVisit.getText())) {
                                        syntagografisi.setReasonOfVisit(reasonOfVisit.getText());
                                    } else {
                                        syntagografisi.setReasonOfVisit("�������");
                                    }
                                    try {
                                        frame.dispose();
                                        syntagografisi.startVisit();
                                    } catch (Exception e) {
                                        JOptionPane.showMessageDialog(EOPYYForm.this, "������ �� ������� ������� ��������");
                                        ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ���� ��� ���� ���������");
                                }
                                return null;
                            }
                        };
                        worker.execute();
                    });
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                    ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                    restartApplication();
                }
            }
        });
        return ivjJButton2;
    }

    private JButton startExaminations() {
        if (ivjJButton3 == null) {
            ivjJButton3 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton3.getFont().getName(), ivjJButton3.getFont().getStyle(), 14);
        ivjJButton3.setFont(newButtonFont);
        ivjJButton3.setText("�������� ������ �������������");
        ivjJButton3.addActionListener(ae -> {
            final JDialog d = new JDialog(EOPYYForm.this, "������������...", Dialog.ModalityType.DOCUMENT_MODAL);
            JPanel p1 = new JPanel(new GridBagLayout());
            p1.add(new JLabel("�������� ������ �������������..."), new GridBagConstraints());
            d.getContentPane().add(p1);
            d.setSize(400, 100);
            d.setLocationRelativeTo(ivjJFrameContentPane);
            d.setDefaultCloseOperation(0);
            d.setModal(true);
            d.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent ev) {
                    d.dispose();
                }
            });
            SwingWorker<Void, Void> worker = new SwingWorker() {
                protected Void doInBackground()
                        throws Exception {
                    try {
                        int diseaseNumber = -1;
                        for (int i = 0; i < jCheckBoxList.size(); i++) {
                            if (jCheckBoxList.get(i).isSelected()) {
                                diseaseNumber = i;
                                break;
                            }
                        }
                        if (diseaseNumber != -1) {
                            Thread.sleep(2000L);
                            d.dispose();
                            syntagografisi.setDiseaseNumber(diseaseNumber);
                            syntagografisi.startFavoriteExaminations();
                        } else {
                            d.dispose();
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ��������");
                        }
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                        ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                        restartApplication();
                    }
                    return null;
                }
            };
            worker.execute();
            d.setVisible(true);
        });
        return ivjJButton3;
    }

    private JButton startExaminationsWithoutDisease() {
        if (ivjJButton11 == null) {
            ivjJButton11 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton11.getFont().getName(), ivjJButton11.getFont().getStyle(), 14);
        ivjJButton11.setFont(newButtonFont);
        ivjJButton11.setText("���������� �������������");
        ivjJButton11.addActionListener(ae -> {
            final JDialog d = new JDialog(EOPYYForm.this, "������������", Dialog.ModalityType.DOCUMENT_MODAL);
            JPanel p1 = new JPanel(new GridBagLayout());
            p1.add(new JLabel("�������� �������������..."), new GridBagConstraints());
            d.getContentPane().add(p1);
            d.setSize(400, 100);
            d.setLocationRelativeTo(ivjJFrameContentPane);
            d.setDefaultCloseOperation(0);
            d.setModal(true);
            d.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent ev) {
                    d.dispose();
                }
            });
            SwingWorker<Void, Void> worker = new SwingWorker() {
                protected Void doInBackground()
                        throws Exception {
                    try {
                        Thread.sleep(2000L);
                        d.dispose();
                        syntagografisi.startExaminatiosWithoutDisease();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                        ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                        restartApplication();
                    }
                    return null;
                }
            };
            worker.execute();
            d.setVisible(true);
        });
        return ivjJButton11;
    }

    private JButton searchPrescription() {
        if (ivjJButton12 == null) {
            ivjJButton12 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton12.getFont().getName(), ivjJButton12.getFont().getStyle(), 14);
        ivjJButton12.setFont(newButtonFont);
        ivjJButton12.setText("��������� ��������");
        ivjJButton12.addActionListener(ae -> {
            if (syntagografisi != null) {
                try {
                    final JFrame frame = new JFrame("��������� ��������");
                    frame.setTitle("��������� ��������");
                    JButton insert = new JButton("���������");
                    JButton bClose = new JButton("��������");
                    JButton status = new JButton("���������");
                    JPanel checkPanel = new JPanel(new GridLayout(2, 2));
                    JLabel label = new JLabel("������ ��������");
                    Font newButtonFont1 = new Font(label.getFont().getName(), label.getFont().getStyle(), 14);
                    label.setFont(newButtonFont1);
                    id = new JTextField(40);
                    checkPanel.add(label);
                    checkPanel.add(id);
                    JPanel p1 = new JPanel();
                    p1.add(insert);
                    p1.add(status);
                    p1.add(bClose);
                    frame.getContentPane().add(p1, "Last");
                    frame.getContentPane().add(checkPanel, "Center");
                    frame.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent ev) {
                            frame.dispose();
                        }
                    });
                    frame.setSize(350, 120);
                    Toolkit toolkit = Toolkit.getDefaultToolkit();
                    Dimension screenSize = toolkit.getScreenSize();
                    int x1 = (screenSize.width - frame.getWidth()) / 2;
                    int y1 = (screenSize.height - frame.getHeight()) / 2;
                    frame.setLocation(x1, y1);
                    frame.setVisible(true);
                    frame.setResizable(false);
                    bClose.addActionListener(ae1 -> frame.dispose());
                    status.addActionListener(ae12 -> {
                        if ((ValidationUtil.fieldPresent(id.getText())) && (id.getText().length() > 6)) {
                            syntagografisi.setId(id.getText().trim());
                            try {
                                frame.dispose();
                                syntagografisi.statusPrescription();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(EOPYYForm.this, "�Ѽ����� ��������ͺ��");
                                ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                            }
                        } else {
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������ �������� ");
                        }
                    });
                    insert.addActionListener(ae13 -> {
                        if ((ValidationUtil.fieldPresent(id.getText())) && (id.getText().length() > 6)) {
                            syntagografisi.setId(id.getText().trim());
                            try {
                                frame.dispose();
                                syntagografisi.searchPrescription();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(EOPYYForm.this, "�Ѽ����� ��������ͺ��");
                                ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                            }
                        } else {
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������ ��������");
                        }
                    });
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(EOPYYForm.this, "�Ѽ����� ��������ͺ��");
                    ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                    restartApplication();
                }
            }
        });
        return ivjJButton12;
    }

    private JButton searchExaminations() {
        if (ivjJButton13 == null) {
            ivjJButton13 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton13.getFont().getName(), ivjJButton13.getFont().getStyle(), 14);
        ivjJButton13.setFont(newButtonFont);
        ivjJButton13.setText("��������� �������������");
        ivjJButton13.addActionListener(ae -> {
            if (syntagografisi != null) {
                try {
                    final JFrame frame = new JFrame("��������� �������������");
                    frame.setTitle("��������� �������������");

                    JButton insert = new JButton("���������");
                    JButton bClose = new JButton("��������");
                    JButton status = new JButton("���������");

                    JPanel checkPanel = new JPanel(new GridLayout(2, 2));
                    JLabel label = new JLabel("������� �������������");

                    Font newButtonFont1 = new Font(label.getFont().getName(), label.getFont().getStyle(), 14);
                    label.setFont(newButtonFont1);

                    id = new JTextField(40);
                    checkPanel.add(label);
                    checkPanel.add(id);

                    JPanel p1 = new JPanel();
                    p1.add(insert);
                    p1.add(status);
                    p1.add(bClose);

                    frame.getContentPane().add(p1, "Last");
                    frame.getContentPane().add(checkPanel, "Center");
                    frame.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent ev) {
                            frame.dispose();
                        }
                    });

                    frame.setSize(350, 120);
                    Toolkit toolkit = Toolkit.getDefaultToolkit();
                    Dimension screenSize = toolkit.getScreenSize();

                    int x1 = (screenSize.width - frame.getWidth()) / 2;
                    int y1 = (screenSize.height - frame.getHeight()) / 2;
                    frame.setLocation(x1, y1);
                    frame.setVisible(true);
                    frame.setResizable(false);

                    bClose.addActionListener(ae1 -> frame.dispose());

                    status.addActionListener(ae12 -> {
                        if ((ValidationUtil.fieldPresent(id.getText())) && (id.getText().length() > 6)) {
                            syntagografisi.setId(id.getText().trim());
                            try {
                                frame.dispose();
                                syntagografisi.statusExams();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                                ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                            }
                        } else {
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������ �������������");
                        }
                    });

                    insert.addActionListener(ae1 -> {
                        if ((ValidationUtil.fieldPresent(id.getText())) && (id.getText().length() > 6)) {
                            syntagografisi.setId(id.getText().trim());
                            try {
                                frame.dispose();
                                syntagografisi.searchExams();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                                ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                            }
                        } else {
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������ �������������");
                        }
                    });
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                    ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                    restartApplication();
                }
            }
        });
        return ivjJButton13;
    }

    private JButton startApplication() {
        if (ivjJButton1 == null) {
            ivjJButton1 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton1.getFont().getName(), ivjJButton1.getFont().getStyle(), 14);
        ivjJButton1.setFont(newButtonFont);
        ivjJButton1.setText("��������� ��������������");
        ivjJButton1.addActionListener(ae -> {
            if (syntagografisi != null) {
                final JDialog d = new JDialog(EOPYYForm.this, "������������..", Dialog.ModalityType.DOCUMENT_MODAL);
                JPanel p1 = new JPanel(new GridBagLayout());
                p1.add(new JLabel("� �������������� ��������..."), new GridBagConstraints());
                d.getContentPane().add(p1);
                d.setSize(400, 100);
                d.setLocationRelativeTo(ivjJFrameContentPane);
                d.setDefaultCloseOperation(0);
                d.setModal(true);
                d.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent ev) {
                        d.dispose();
                    }
                });
                SwingWorker<Void, Void> worker = new SwingWorker() {
                    protected Void doInBackground()
                            throws Exception {
                        try {
                            try {
                                syntagografisi.tearDown();
                            } catch (Exception e) {
                                ExecuteEOPYY.timerLogger.info(PrintUtil.formatThrowableStackTrace(e));
                            }
                            syntagografisi.initiate();
                            d.dispose();
                            syntagografisi.startSyntagografisi();
                        } catch (Exception e) {
                            d.dispose();
                            JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                            ExecuteEOPYY.timerLogger.info(PrintUtil.formatThrowableStackTrace(e));
                            restartApplication();
                        }
                        return null;
                    }
                };
                worker.execute();
                d.setVisible(true);
            }
        });
        return ivjJButton1;
    }

    private void restartApplication() {
        if (syntagografisi != null) {
            final JDialog d = new JDialog(this, "������������", Dialog.ModalityType.DOCUMENT_MODAL);
            JPanel p1 = new JPanel(new GridBagLayout());
            p1.add(new JLabel("� �������������� ��������"), new GridBagConstraints());
            d.getContentPane().add(p1);
            d.setSize(400, 100);
            d.setLocationRelativeTo(ivjJFrameContentPane);
            d.setDefaultCloseOperation(0);
            d.setModal(true);
            d.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent ev) {
                    d.dispose();
                }
            });
            SwingWorker<Void, Void> worker = new SwingWorker() {
                protected Void doInBackground()
                        throws Exception {
                    try {
                        try {
                            syntagografisi.tearDown();
                        } catch (Exception e) {
                            ExecuteEOPYY.timerLogger.info(PrintUtil.formatThrowableStackTrace(e));
                        }
                        syntagografisi.initiate();
                        d.dispose();
                        syntagografisi.startSyntagografisi();
                    } catch (Exception e) {
                        d.dispose();
                        JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                        ExecuteEOPYY.timerLogger.info(PrintUtil.formatThrowableStackTrace(e));
                        restartApplication();
                    }
                    return null;
                }
            };
            worker.execute();
            d.setVisible(true);
        }
    }

    private void addSelectedAilment(JCheckBox jCheckBox, boolean isSelected) {
        Pattern p = Pattern.compile("((\\d+))");
        Matcher m = p.matcher(jCheckBox.getText());
        if (isSelected) {
            if (m.find()) {
                prescriptionDiagnosis.add(Integer.valueOf(Integer.valueOf(m.group(1)).intValue() - 1));
            }
        } else if (m.find()) {
            List<Integer> removedList = new ArrayList();
            for (Integer prescription : prescriptionDiagnosis) {
                if (prescription.equals(Integer.valueOf(Integer.valueOf(m.group(1)).intValue() - 1))) {
                    removedList.add(prescription);
                }
            }
            prescriptionDiagnosis.removeAll(removedList);
        }
    }

    private JButton startPresription() {
        if (ivjJButton10 == null) {
            ivjJButton10 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton10.getFont().getName(), ivjJButton10.getFont().getStyle(), 14);
        ivjJButton10.setFont(newButtonFont);
        ivjJButton10.setText("�������� ��������");
        ivjJButton10.addActionListener(ae -> {
            prescriptionDiagnosis = new ArrayList();
            if (syntagografisi != null) {
                try {
                    final JFrame frame = new JFrame("�������");
                    frame.setTitle("�������� ��������");
                    JButton insert = new JButton("��������");
                    JButton bClose = new JButton("��������");
                    JPanel p1 = new JPanel();
                    p1.add(insert);
                    p1.add(bClose);
                    JPanel ivjJPanel2 = new JPanel();
                    JPanel checkPanel = new JPanel(new GridLayout(syntagografisi.getAilments().getAilments().size() - 1,
                            syntagografisi.getAilments().getAilments().size() - 1));

                    JLabel nameLabel2 = new JLabel("   ".concat("������� �������"));
                    Font nameLabel2Font = new Font(nameLabel2.getFont().getName(), nameLabel2.getFont().getStyle(), 16);
                    nameLabel2.setFont(nameLabel2Font);
                    checkPanel.add(nameLabel2);
                    checkPanel.add(new JLabel());
                    char character = syntagografisi.getAilments().getAilments().get(0).getDescription().charAt(0);
                    JLabel characterLabel = new JLabel("   " + String.valueOf(character));
                    Font characterLabelFont = new Font(characterLabel.getFont().getName(), characterLabel.getFont().getStyle(), 18);
                    characterLabel.setFont(characterLabelFont);
                    checkPanel.add(characterLabel);
                    checkPanel.add(new JLabel());
                    int count = 0;
                    for (int i = 0; i < syntagografisi.getAilments().getAilments().size(); i++) {
                        if (syntagografisi.getAilments().getAilments().get(i).getDescription().charAt(0) != character) {
                            character = syntagografisi.getAilments().getAilments().get(i).getDescription().charAt(0);
                            characterLabel = new JLabel("   " + String.valueOf(character));
                            characterLabelFont = new Font(characterLabel.getFont().getName(), characterLabel.getFont().getStyle(), 18);
                            characterLabel.setFont(characterLabelFont);
                            if (count % 2 == 0) {
                                checkPanel.add(characterLabel);
                                checkPanel.add(new JLabel());
                            } else {
                                checkPanel.add(new JLabel());
                                checkPanel.add(characterLabel);
                                checkPanel.add(new JLabel());
                            }
                            count = 0;
                        }
                        JCheckBox ailment = null;
                        if (i + 1 >= 10) {
                            ailment = new JCheckBox("("
                                    .concat(syntagografisi.getAilments().getAilments().get(i).getNumber())
                                    .concat(").     ")
                                    .concat(syntagografisi.getAilments().getAilments().get(i).getDescription()));
                        } else {
                            ailment = new JCheckBox("(" +
                                    syntagografisi.getAilments().getAilments().get(i).getNumber() + ")." + "      " +
                                    syntagografisi.getAilments().getAilments().get(i).getDescription());
                        }
                        ailment.setMnemonic(72);
                        ailment.setPreferredSize(new Dimension(33, 28));
                        ailment.addItemListener(e -> {
                            if (e.getStateChange() == 1) {
                                addSelectedAilment((JCheckBox) e.getItem(), true);
                            } else {
                                addSelectedAilment((JCheckBox) e.getItem(), false);
                            }
                        });
                        checkPanel.add(ailment);
                        ailmentsList.add(ailment);
                        count++;
                    }
                    JScrollPane scroll = new JScrollPane(checkPanel, 20, 32);

                    scroll.setPreferredSize(new Dimension(1200, 600));
                    scroll.setAutoscrolls(true);
                    ivjJPanel2.add(scroll);
                    frame.getContentPane().add(p1, "Last");
                    frame.getContentPane().add(ivjJPanel2, "Center");
                    frame.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent ev) {
                            frame.dispose();
                        }
                    });
                    frame.setSize(1200, 680);
                    Toolkit toolkit = Toolkit.getDefaultToolkit();
                    Dimension screenSize = toolkit.getScreenSize();
                    int x = (screenSize.width - frame.getWidth()) / 2;
                    int y = (screenSize.height - frame.getHeight()) / 2;
                    frame.setLocation(x, y);
                    frame.setVisible(true);
                    frame.setResizable(false);
                    bClose.addActionListener(ae1 -> frame.dispose());
                    insert.addActionListener(ae12 -> {
                        SwingWorker<Void, Void> worker = new SwingWorker() {
                            protected Void doInBackground()
                                    throws Exception {
                                try {
                                    syntagografisi.setPrescriptionDiagnosis(prescriptionDiagnosis);
                                    frame.dispose();
                                    syntagografisi.startPresription();
                                } catch (Exception e) {
                                    JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                                    ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                                    restartApplication();
                                }
                                return null;
                            }
                        };
                        worker.execute();
                    });
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                    ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                    restartApplication();
                }
            }
        });
        return ivjJButton10;
    }

    private JButton closeVisit() {
        if (ivjJButton9 == null) {
            ivjJButton9 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton9.getFont().getName(), ivjJButton9.getFont().getStyle(), 14);
        ivjJButton9.setFont(newButtonFont);
        ivjJButton9.setText("�������� ���������");
        ivjJButton9.addActionListener(ae -> {
            final JDialog d = new JDialog(EOPYYForm.this, "������������", Dialog.ModalityType.DOCUMENT_MODAL);
            JPanel p1 = new JPanel(new GridBagLayout());
            p1.add(new JLabel("�������� ���������..."), new GridBagConstraints());
            d.getContentPane().add(p1);
            d.setSize(400, 100);
            d.setLocationRelativeTo(ivjJFrameContentPane);
            d.setDefaultCloseOperation(0);
            d.setModal(true);
            d.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent ev) {
                    d.dispose();
                }
            });
            SwingWorker<Void, Void> worker = new SwingWorker() {
                protected Void doInBackground()
                        throws Exception {
                    try {
                        Thread.sleep(2000L);
                        d.dispose();
                        syntagografisi.closeVisit();
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(EOPYYForm.this, "�������� ������������");
                        ExecuteEOPYY.diagnosisTimer.info(PrintUtil.formatThrowableStackTrace(e));
                        restartApplication();
                    }
                    return null;
                }
            };
            worker.execute();
            d.setVisible(true);
        });
        return ivjJButton9;
    }

    private JButton insertPersonalDetails() {
        if (ivjJButton7 == null) {
            ivjJButton7 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton7.getFont().getName(), ivjJButton7.getFont().getStyle(), 14);

        ivjJButton7.setFont(newButtonFont);

        ivjJButton7.setText("�������� ���������� ���������..");
        ivjJButton7.addActionListener(ae -> {
            Logon logonDAO = new Logon();
            logonDAO.startLogon();
        });
        return ivjJButton7;
    }

    private JButton displayExams() {
        if (ivjJButton8 == null) {
            ivjJButton8 = new JButton();
        }
        Font newButtonFont = new Font(ivjJButton8.getFont().getName(), ivjJButton8.getFont().getStyle(), 14);

        ivjJButton8.setFont(newButtonFont);

        ivjJButton8.setText("�������� ���������..");
        ivjJButton8.addActionListener(ae -> {
            Thread t = new Thread() {
                public void run() {
                    displayDiseases.setExaminations(ExamsUtil.getExamsAsObject());
                    displayDiseases.startLogon();
                }
            };
            t.start();
        });
        return ivjJButton8;
    }

    private JPanel getIvjJPanel() {
        if (ivjJPanel == null) {
            ivjJPanel = new JPanel();
        }
        JPanel checkPanel = new JPanel(new GridLayout(jCheckBoxList.size() - 1, jCheckBoxList.size() - 1));
        ButtonGroup group2 = new ButtonGroup();
        for (int i = 0; i < jCheckBoxList.size(); i++) {
            jCheckBoxList.get(i).setMnemonic(72);
            jCheckBoxList.get(i).setPreferredSize(new Dimension(28, 23));
            checkPanel.add(jCheckBoxList.get(i));
            group2.add(jCheckBoxList.get(i));
        }
        JScrollPane scroll = new JScrollPane(checkPanel, 20, 32);
        JPanel jpanel2 = new JPanel();
        jpanel2.setPreferredSize(new Dimension(300, 290));
        scroll.setPreferredSize(new Dimension(900, 550));
        scroll.setAutoscrolls(true);
        ivjJPanel.add(scroll);
        ivjJPanel.add(jpanel2);
        return ivjJPanel;
    }
}
