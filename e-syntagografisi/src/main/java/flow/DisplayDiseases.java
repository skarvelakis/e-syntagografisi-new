package flow;

import internal.Category;
import internal.Disease;
import internal.Exam;
import internal.Examinations;
import utilities.ValidationUtil;

import javax.swing.*;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class DisplayDiseases implements ActionListener {

    private JButton edit;
    private JButton bClose;
    private Examinations exams;
    private List<JCheckBox> jCheckBoxList = null;
    private JFrame frame;
    private JFrame frame1;
    private static JEditorPane editorPane;
    private static HTMLEditorKit htmlEditorKit;
    private static StyleSheet styleSheet;

    public DisplayDiseases(Examinations examinations) {

        this.jCheckBoxList = new ArrayList();
        this.exams = examinations;
        this.editorPane = new JEditorPane();
        this.editorPane.setContentType("text/html");
        this.editorPane.setEditable(false);
        this.htmlEditorKit = (HTMLEditorKit) editorPane.getEditorKit();
        this.styleSheet = htmlEditorKit.getStyleSheet();

        for (int i = 0; i < exams.getDiseases().size(); i++) {
            if (i + 1 >= 10) {
                this.jCheckBoxList.add(new JCheckBox("("
                        .concat(exams.getDiseases().get(i).getNumber())
                        .concat(").    ")
                        .concat(exams.getDiseases().get(i).getIllness())));
            } else {
                this.jCheckBoxList.add(new JCheckBox("("
                        .concat(exams.getDiseases().get(i).getNumber())
                        .concat(").      ")
                        .concat(exams.getDiseases().get(i).getIllness())));
            }
        }
    }

    public void setExaminations(Examinations examinations) {
        jCheckBoxList = new ArrayList();
        exams = examinations;
        for (int i = 0; i < exams.getDiseases().size(); i++) {
            if (i + 1 >= 10) {
                jCheckBoxList.add(new JCheckBox("("
                        .concat(exams.getDiseases().get(i).getNumber()
                                .concat(").    ")
                                .concat(exams.getDiseases().get(i).getIllness()))));
            } else {
                jCheckBoxList.add(new JCheckBox("("
                        .concat(exams.getDiseases().get(i).getNumber())
                        .concat(").      ")
                        .concat(exams.getDiseases().get(i).getIllness())));
            }
        }
    }

    public void startLogon() {

        frame = new JFrame("���������");
        frame.setTitle("���������");
        edit = new JButton("�������� ���������");
        bClose = new JButton("��������");
        edit.addActionListener(this);
        bClose.addActionListener(this);
        JPanel ivjJPanel = new JPanel();

        JPanel checkPanel = new JPanel(new GridLayout(this.jCheckBoxList.size() - 1, jCheckBoxList.size() - 1));
        checkPanel.add(new JLabel());
        checkPanel.add(new JLabel());
        ButtonGroup group2 = new ButtonGroup();

        for (int i = 0; i < jCheckBoxList.size(); i++) {
            jCheckBoxList.get(i).setMnemonic(72);
            jCheckBoxList.get(i).setToolTipText(String.valueOf(i + 1 + " ��������"));
            jCheckBoxList.get(i).setPreferredSize(new Dimension(65, 28));
            checkPanel.add(jCheckBoxList.get(i));
            group2.add(jCheckBoxList.get(i));
        }

        JScrollPane scroll = new JScrollPane(checkPanel, 22, 30);
        scroll.setPreferredSize(new Dimension(1000, 400));
        scroll.setAutoscrolls(true);
        ivjJPanel.add(scroll);
        JPanel p1 = new JPanel();
        p1.add(this.edit);
        p1.add(this.bClose);

        frame.getContentPane().add(p1, "Center");
        frame.getContentPane().add(ivjJPanel, "North");

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                DisplayDiseases.this.frame.dispose();
            }
        });
        frame.setSize(1200, 500);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int x = (screenSize.width - frame.getWidth()) / 2;
        int y = (screenSize.height - frame.getHeight()) / 2;
        frame.setLocation(x, y);
        frame.setVisible(true);
        frame.setResizable(false);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("�������� ���������")) {
            Integer diseaseN = null;
            for (int i = 0; i < jCheckBoxList.size(); i++) {
                if (jCheckBoxList.get(i).isSelected()) {
                    diseaseN = Integer.valueOf(i);
                    break;
                }
            }
            if (ValidationUtil.fieldPresent(diseaseN)) {
                frame1 = new JFrame();
                frame1.setTitle(exams.getDiseases().get(diseaseN.intValue()).getIllness());
                JButton bClose1 = new JButton("�������� ���������");
                bClose1.addActionListener(this);
                JPanel ivjJPanel = new JPanel();
                styleSheet.addRule(String.format("ul{margin:0px 20px;font:italic bold 12px/30px Georgia, serif;", new Object[0]));
                styleSheet.addRule(String.format("body{background-color:#DCDCDC;", new Object[0]));
                String htmlText = "<html><ul>";
                for (Category category : exams.getDiseases().get(diseaseN.intValue()).getCategories()) {
                    for (Exam exam : category.getExams()) {
                        htmlText = htmlText + "<li>" + exam.getDescription() + "</li>";
                    }
                }
                htmlText = htmlText + "</ul></html>";
                editorPane.setText(htmlText);
                JScrollPane scroll = new JScrollPane(editorPane, 22, 30);
                scroll.setPreferredSize(new Dimension(1000, 445));
                scroll.setAutoscrolls(true);
                ivjJPanel.add(scroll);
                JPanel p1 = new JPanel();
                p1.add(bClose1);
                frame1.getContentPane().add(p1, "Center");
                frame1.getContentPane().add(ivjJPanel, "North");
                frame1.addWindowListener(new WindowAdapter() {
                    public void windowClosing(WindowEvent ev) {
                        DisplayDiseases.this.frame1.dispose();
                    }
                });

                frame1.setSize(1200, 530);
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screenSize = toolkit.getScreenSize();
                int x = (screenSize.width - frame.getWidth()) / 2;
                int y = (screenSize.height - frame.getHeight()) / 2;
                frame1.setLocation(x, y);
                frame1.setVisible(true);
                frame1.setResizable(false);
            } else {
                JOptionPane.showMessageDialog(this.frame1, "�������� ��������");
            }
        }
        if (e.getActionCommand().equals("��������")) {
            frame.dispose();
        }
        if (e.getActionCommand().equals("�������� ���������")) {
            frame1.dispose();
        }
    }

    public Examinations getExams() {
        return exams;
    }

    public void setExams(Examinations exams) {
        this.exams = exams;
    }
}
