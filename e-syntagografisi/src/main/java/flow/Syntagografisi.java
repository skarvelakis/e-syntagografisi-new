package flow;

import com.google.common.base.Function;
import internal.*;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.pagefactory.ByChained;
import org.openqa.selenium.support.ui.*;
import utilities.PropertiesManager;
import utilities.ValidationUtil;

import javax.swing.*;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Syntagografisi implements Runnable {

    private WebDriver syntagografisiDriver;
    private String baseUrl;
    private StringBuffer verificationErrors = new StringBuffer();
    private String syntagografisiProperty;
    private int seconds;
    private Timer timer;
    private String amka;
    private String id;
    private PropertiesManager propertyManager;
    private String debugMode = null;
    private int diseaseNumber;
    private String reasonOfVisit;
    private Examinations examinations;
    private Ailments ailments;
    private List<Integer> prescriptionDiagnosis;
    private Integer secSyntagografisi;


    private static final String DOCTOR_VISIT = "DoctorVisit";
    private static final String DOCTOR_ADD_EXAMINATION = "DoctorAddExamination";
    private static final String ADD_PRESCRIPTION = "AddPrescription";
    private static final String ANOSOSFAIRINI = "���������� ������������� �������������� IgG, IgA, IgM";
    private static final String TRANSAMINASEIS = "������������� ������� ������ (SGPT-SGOT)";
    private static final String ISXIA = "�������������� (u/s) ������ �������";
    private static final String KALLIERGEIA = "����������� �����������";
    private static final String TOXOPLASMA = "������� ������� ��� ��������� ����������� ������ TOXOPLASMA";
    private static final String THORAKOS = "����������, ������ ���� - ���� (�������): �) �������";
    private static final String KOILIA = "������������ �������";
    private static final String MALAKVN_MORIWN = "�������������� (u/s) ������� ������";
    private static final String KREATAKIA = "������������ ������� ������� ������";
    private static final String GASTRENTERITIDA = "����������� ��������";


    public Syntagografisi(String syntagografisiProperty, String baseUrl) {
        this.syntagografisiProperty = syntagografisiProperty;
        this.baseUrl = baseUrl;
    }

    public synchronized void initiate() throws Exception {
        System.setProperty("webdriver.firefox.bin", syntagografisiProperty);
        DesiredCapabilities caps = DesiredCapabilities.firefox();
        caps.setCapability("handlesAlerts", Boolean.TRUE);
        try {
            syntagografisiDriver.manage().window().maximize();
        } catch (Exception e) {
            syntagografisiDriver = new FirefoxDriver(caps);
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver.get(baseUrl);
        }
    }

    public void closeVisit() throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            String url = syntagografisiDriver.getCurrentUrl();
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver)
                    .withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
            if (!url.contains(DOCTOR_VISIT)) {
                syntagografisiDriver.findElement(By.id("pt1:visitBtn")).click();
                try {
                    syntagografisiDriver.switchTo().alert().accept();
                    syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                } catch (Exception e) {
                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                }
            }
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'�������� ���������')]")));
            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'�������� ���������')]")).click();
            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:r1:0:cb10")));
            syntagografisiDriver.findElement(By.id("pt1:r1:0:cb10")).click();

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'��� ��������')]")));
            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'��� ��������')]")).click();
            syntagografisiDriver.switchTo().alert().accept();
        } catch (Exception ex) {
            ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
        }
    }

    public void startVisit() throws Exception {
        try {
            syntagografisiDriver.manage().timeouts().implicitlyWait(3L, TimeUnit.SECONDS);
            syntagografisiDriver.manage().window().maximize();
            String url = syntagografisiDriver.getCurrentUrl();
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            if (!url.contains(DOCTOR_VISIT)) {
                syntagografisiDriver.findElement(By.id("pt1:visitBtn")).click();
                try {
                    syntagografisiDriver.switchTo().alert().accept();
                    syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                } catch (Exception e) {
                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                }
            }

            try {
                if (syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'�������� ���������')]")).isDisplayed()) {
                    syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'�������� ���������')]")).click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:r1:0:cb10")));
                    syntagografisiDriver.findElement(By.id("pt1:r1:0:cb10")).click();
                }
            } catch (Exception ex) {
                ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            }

            try {
                if (syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'��� ��������')]")).isDisplayed()) {
                    syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'��� ��������')]")).click();
                }
            } catch (Exception ex) {
                ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            }

            try {
                syntagografisiDriver.switchTo().alert().accept();
            } catch (Exception ex) {
                ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(), '���� ������������')]")));
            WebElement webElement = syntagografisiDriver.findElement(By.xpath("//label[contains(text(), '���� ������������')]"));
            syntagografisiDriver.findElement(By.id(webElement.getAttribute("for"))).sendKeys(amka);
            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'���������')]")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:r1:0:r1:0:patientInfo")));

            try {
                if (syntagografisiDriver.findElement(By.id("pt1:r1:0:r1:0:insurChangePopup::1415645672667")).isDisplayed()) {
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("pt1:r1:0:r1:0:insurChangePopup::1415645672667")));
                }
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//label[contains(text(), '����� ���������')]")));
            webElement = syntagografisiDriver.findElement(By.xpath("//label[contains(text(), '����� ���������')]"));
            syntagografisiDriver.findElement(By.id(webElement.getAttribute("for"))).sendKeys(reasonOfVisit.toUpperCase());
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'����������')]")));
            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'����������')]")).click();

        } catch (Exception ex) {
            ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
        } finally {
        }
    }

    public void startFavoriteExaminations() throws Exception {

        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            String url = syntagografisiDriver.getCurrentUrl();
            if (!url.contains(DOCTOR_ADD_EXAMINATION)) {
                syntagografisiDriver.findElement(By.id("pt1:addExamBtn")).click();
                try {
                    syntagografisiDriver.switchTo().alert().accept();
                    syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                } catch (Exception e) {
                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                }
            }
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            try {
                if (syntagografisiDriver.findElement(By.id("pt1:r1:0:cb3")).isEnabled()) {
                    syntagografisiDriver.findElement(By.id("pt1:r1:0:cb3")).click();
                    wait.until((Function) driver -> {
                        ByChained chained = new ByChained(By.id("pt1:r1:0:examsTb::db"), By.tagName("table"), By.tagName("tr"));
                        List<WebElement> resultsRows = syntagografisiDriver.findElements(chained);
                        return Boolean.valueOf(resultsRows.isEmpty());
                    });
                }
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }
            Disease disease = examinations.getDiseases().get(diseaseNumber);
            String description = "";

            if (ValidationUtil.fieldPresent(disease)) {
                description = disease.getIllness();
                String pattern = "(\\([A-Za-z�-��-�]\\))";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(description);
                if (m.find()) {
                    description = m.replaceAll("");
                }
            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:r1:0:cb6")));
            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:r1:0:it1::content")));
            syntagografisiDriver.findElement(By.id("pt1:r1:0:it1::content")).sendKeys(description);
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:r1:0:it1::content"), description));
            WebElement e = syntagografisiDriver.findElement(By.id("pt1:r1:0:soc2::content"));
            Select select = new Select(e);
            select.selectByVisibleText(String.valueOf(disease.getCategories().get(0).getCategoryNumber()));
            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:r1:0:cb5")));
            syntagografisiDriver.findElement(By.id("pt1:r1:0:cb5")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")));

            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(syntagografisiDriver.findElement(
                    By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")).getAttribute("id")));

            boolean needsComments = false;
            WebElement webElement = null;

            for (Exam exam : disease.getCategories().get(0).getExams()) {
                try {
                    int pageNumber = Integer.valueOf(exam.getPageNumber()).intValue();
                    if (pageNumber != 25) {
                        webElement = syntagografisiDriver.findElement(By.id("favTb::scroller"));
                        webElement.sendKeys(Keys.chord(Keys.PAGE_DOWN, "a"));
                        webElement.sendKeys(Keys.chord(Keys.PAGE_DOWN, "a"));
                        Thread.sleep(1500L);
                    }
                    if (exam.getXPath().startsWith("@@")) {
                        wait.until(ExpectedConditions.elementToBeClickable(By.id(exam.getXPath().replace("@", ""))));
                        webElement = syntagografisiDriver.findElement(By.id(exam.getXPath().replace("@", "")));
                    } else {
                        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(exam.getXPath())));
                        webElement = syntagografisiDriver.findElement(By.xpath(exam.getXPath()));
                    }
                    if (exam.getDescription().equalsIgnoreCase(ANOSOSFAIRINI)) {
                        webElement = syntagografisiDriver.findElement(By.xpath(exam.getxPathNr()));
                        webElement.clear();
                        webElement.sendKeys("3");
                        needsComments = true;
                    }
                    if (exam.getDescription().equalsIgnoreCase(TRANSAMINASEIS)) {
                        webElement = syntagografisiDriver.findElement(By.xpath(exam.getxPathNr()));
                        webElement.clear();
                        webElement.sendKeys("2");
                    } else if ((exam.getDescription().equalsIgnoreCase(ISXIA))
                            || (exam.getDescription().equalsIgnoreCase(TOXOPLASMA))
                            || (exam.getDescription().equalsIgnoreCase(THORAKOS))) {
                        webElement = syntagografisiDriver.findElement(By.xpath(exam.getxPathNr()));
                        webElement.clear();
                        webElement.sendKeys("2");
                        needsComments = true;
                    } else if ((exam.getDescription().contains(KALLIERGEIA))
                            || (exam.getDescription().contains(KOILIA))
                            || (exam.getDescription().contains(KREATAKIA))
                            || (exam.getDescription().contains(GASTRENTERITIDA))) {
                        needsComments = true;
                    } else if (exam.getDescription().contains(MALAKVN_MORIWN)) {
                        needsComments = true;
                        if (description.contains("������")) {
                            webElement = syntagografisiDriver.findElement(By.xpath(exam.getxPathNr()));
                            webElement.clear();
                            webElement.sendKeys("2");
                        }
                    }
                    webElement.click();

                } catch (Exception ex) {
                    ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
                }
            }
            Thread.sleep(1000L);
            syntagografisiDriver.findElement(By.id("cb2")).click();
            if (needsComments) {
                ByChained chained = new ByChained(By.id("pt1:r1:0:examsTb::db"), By.tagName("table"), By.tagName("tr"));
                wait.until(ExpectedConditions.visibilityOfElementLocated(chained));
                syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                List<?> resultsRows = syntagografisiDriver.findElements(chained);
                Iterator<?> elementsIterators = resultsRows.iterator();

                while (elementsIterators.hasNext()) {
                    try {
                        WebElement elementIterator = (WebElement) elementsIterators.next();
                        List<?> sessionData = elementIterator.findElements(By.tagName("td"));
                        if (((WebElement) sessionData.get(0)).getText().contains(ANOSOSFAIRINI)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� �������������� IgG");
                            elementIterator = (WebElement) elementsIterators.next();
                            sessionData = elementIterator.findElements(By.tagName("td"));
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� �������������� IgA");
                            elementIterator = (WebElement) elementsIterators.next();
                            sessionData = elementIterator.findElements(By.tagName("td"));
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� �������������� IgM");
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(ISXIA)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� ��������� ������");
                            elementIterator = (WebElement) elementsIterators.next();
                            sessionData = elementIterator.findElements(By.tagName("td"));
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� ������ ������");
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(TRANSAMINASEIS)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� SGPT");
                            elementIterator = (WebElement) elementsIterators.next();
                            sessionData = elementIterator.findElements(By.tagName("td"));
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������� SGOT");
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(KALLIERGEIA)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys(description);
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(THORAKOS)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������");
                            elementIterator = (WebElement) elementsIterators.next();
                            sessionData = elementIterator.findElements(By.tagName("td"));
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("FAS");
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(KOILIA)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("����� ����");
                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(MALAKVN_MORIWN)) {
                            if (description.contains("������")) {
                                ((WebElement) sessionData.get(1)).findElement(By.tagName("input")).sendKeys("���������");
                                elementIterator = (WebElement) elementsIterators.next();
                                sessionData = elementIterator.findElements(By.tagName("td"));
                                ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������");
                                break;
                            }
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys(description);

                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(KREATAKIA)) {
                            if (description.equals("�����������������")) {
                                ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������������ ������ ��������");
                                break;
                            }
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("������ ��������� ��� ����������� ��� ������ ���������� ������������");

                            break;
                        }
                        if (((WebElement) sessionData.get(0)).getText().contains(GASTRENTERITIDA)) {
                            ((WebElement) sessionData.get(3)).findElement(By.tagName("input")).sendKeys("��� ������� ��� �������� ��������");
                            break;
                        }
                    } catch (Exception ez1) {
                        ExecuteEOPYY.timerLogger.info(ez1.getLocalizedMessage());
                    }
                }
            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:r1:0:ctb1")));
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            syntagografisiDriver.findElement(By.id("pt1:r1:0:ctb1")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")));

            wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(syntagografisiDriver.findElement(
                    By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")).getAttribute("id")));

            String xpathOfICD = "/html/body/div/form/div/div/div/div/div[1]/div/div/div/div[1]/div/table/tbody/tr/td[2]/div/div[1]/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input";
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathOfICD)));
            syntagografisiDriver.findElement(By.xpath(xpathOfICD)).sendKeys((CharSequence) disease.getICDCodeList().get(0));
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.xpath(xpathOfICD), disease.getICDCodeList().get(0)));
            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'���������')]")).click();
            ByChained byChained = new ByChained(By.id("t1::db"), By.tagName("table"), By.tagName("tr"));
            wait.until(ExpectedConditions.visibilityOfElementLocated(byChained));
            List<WebElement> sessionRows = syntagografisiDriver.findElements(byChained);
            List<WebElement> sessionData = sessionRows.get(0).findElements(By.tagName("td"));
            wait.until(ExpectedConditions.visibilityOf(sessionData.get(0).findElement(By.tagName("input"))));
            sessionData.get(0).findElement(By.tagName("input")).click();

            syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'���������')]")).click();
            Thread.sleep(1500L);

            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            syntagografisiDriver.findElement(By.id("pt1:r1:0:cb2")).sendKeys(Keys.TAB);

            syntagografisiDriver.findElement(By.id("pt1:r1:0:itDiagn:0:cb14")).click();
        } catch (Exception ex) {
            ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
        }
    }

    public void startExaminatiosWithoutDisease() throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            String url = syntagografisiDriver.getCurrentUrl();

            if (!url.contains(DOCTOR_ADD_EXAMINATION)) {
                syntagografisiDriver.findElement(By.id("pt1:addExamBtn")).click();
                try {
                    syntagografisiDriver.switchTo().alert().accept();
                    syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                } catch (Exception e) {
                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                }
            } else {
                syntagografisiDriver.findElement(By.id("pt1:r1:0:cb1")).click();
            }

            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            wait.until(driver -> driver.findElement(By.id("pt1:r1:0:examsTb::db")));
            syntagografisiDriver.findElement(By.id("pt1:r1:0:cb3")).click();

            wait.until((Function) driver -> {
                ByChained chained = new ByChained(By.id("pt1:r1:0:examsTb::db"), By.tagName("table"), By.tagName("tr"));
                List<WebElement> resultsRows = syntagografisiDriver.findElements(chained);
                return Boolean.valueOf(resultsRows.isEmpty());
            });

            syntagografisiDriver.findElement(By.id("pt1:r1:0:it1::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:r1:0:it1::content"), ""));

        } catch (Exception ex) {
            ExecuteEOPYY.timerLogger.info(ex.getLocalizedMessage());
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
        }
    }

    public void searchPrescription() throws Exception {
        try {

            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();

            try {
                syntagografisiDriver.findElement(By.id("pt1:searchPrescriptionBtn")).click();
                syntagografisiDriver.switchTo().alert().accept();
                syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }

            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:prescrPanel::disAcr")));
            syntagografisiDriver.findElement(By.id("pt1:prescrPanel::disAcr")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:it1::content")));
            syntagografisiDriver.findElement(By.id("pt1:it1::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it1::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:it1::content")).sendKeys(id);
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it1::content"), id));
            syntagografisiDriver.findElement(By.id("pt1:id3::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id3::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:id3::content")).sendKeys(id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2));

            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id3::content"), id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2)));

            syntagografisiDriver.findElement(By.id("pt1:cb1")).click();
            wait.until(ExpectedConditions.visibilityOf(syntagografisiDriver.findElement(By.id("pt1:t1::db"))));
            ByChained chained = new ByChained(By.id("pt1:t1::db"), By.tagName("table"), By.tagName("tr"), By.tagName("td"), By.tagName("a"));
            List<WebElement> resultsRows = syntagografisiDriver.findElements(chained);
            if (ValidationUtil.fieldPresent(resultsRows)) {
                resultsRows.get(0).click();
            }
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }

    public void statusPrescription()
            throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            try {
                syntagografisiDriver.findElement(By.id("pt1:searchPrescriptionBtn")).click();
                syntagografisiDriver.switchTo().alert().accept();
                syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:prescrPanel::disAcr")));
            syntagografisiDriver.findElement(By.id("pt1:prescrPanel::disAcr")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:it1::content")));
            syntagografisiDriver.findElement(By.id("pt1:it1::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it1::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:it1::content")).sendKeys(id);
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it1::content"), id));
            syntagografisiDriver.findElement(By.id("pt1:id3::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id3::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:id3::content")).sendKeys(id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2));

            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id3::content"), id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2)));

            syntagografisiDriver.findElement(By.id("pt1:cb1")).click();
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }

    public void searchExams() throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            try {
                syntagografisiDriver.findElement(By.id("pt1:searchPrescriptionBtn")).click();
                syntagografisiDriver.switchTo().alert().accept();
                syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:examPanel::disAcr")));
            syntagografisiDriver.findElement(By.id("pt1:examPanel::disAcr")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:it3::content")));
            syntagografisiDriver.findElement(By.id("pt1:it3::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it3::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:it3::content")).sendKeys(id);
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it3::content"), id));
            syntagografisiDriver.findElement(By.id("pt1:id2::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id2::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:id2::content")).sendKeys(id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2));

            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id2::content"), id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2)));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:cb2")));
            syntagografisiDriver.findElement(By.id("pt1:cb2")).click();
            wait.until(ExpectedConditions.visibilityOf(syntagografisiDriver.findElement(By.id("pt1:t2::db"))));
            ByChained chained = new ByChained(By.id("pt1:t2::db"), By.tagName("table"), By.tagName("tr"), By.tagName("td"), By.tagName("a"));
            List<WebElement> resultsRows = syntagografisiDriver.findElements(chained);
            if (ValidationUtil.fieldPresent(resultsRows)) {
                resultsRows.get(0).click();
            }
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:r1:0:cb2")));
            syntagografisiDriver.findElement(By.id("pt1:r1:0:cb2")).sendKeys(Keys.TAB);
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }

    public void statusExams() throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            try {
                syntagografisiDriver.findElement(By.id("pt1:searchPrescriptionBtn")).click();
                syntagografisiDriver.switchTo().alert().accept();
                syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            }
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:examPanel::disAcr")));
            syntagografisiDriver.findElement(By.id("pt1:examPanel::disAcr")).click();
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:it3::content")));
            syntagografisiDriver.findElement(By.id("pt1:it3::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it3::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:it3::content")).sendKeys(id);
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:it3::content"), id));
            syntagografisiDriver.findElement(By.id("pt1:id2::content")).clear();
            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id2::content"), ""));
            syntagografisiDriver.findElement(By.id("pt1:id2::content")).sendKeys(id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2));

            wait.until(ExpectedConditions.textToBePresentInElementValue(By.id("pt1:id2::content"), id.substring(4, 6) + "/" + id.substring(2, 4) + "/20" + id.substring(0, 2)));

            wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:cb2")));
            syntagografisiDriver.findElement(By.id("pt1:cb2")).click();
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }

    public void startPresription() throws Exception {
        try {
            syntagografisiDriver.manage().window().maximize();
            syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
            String url = syntagografisiDriver.getCurrentUrl();
            Wait<WebDriver> wait = new FluentWait(syntagografisiDriver).withTimeout(2L, TimeUnit.MINUTES).pollingEvery(2L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
            if (!url.contains(ADD_PRESCRIPTION)) {
                syntagografisiDriver.findElement(By.id("pt1:addPrescriptionBtn")).click();
                try {
                    syntagografisiDriver.switchTo().alert().accept();
                    syntagografisiDriver = syntagografisiDriver.switchTo().defaultContent();
                } catch (Exception e) {
                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                }
            } else {
                syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:ClearPrescriptionButton")).click();
            }

            if (ValidationUtil.fieldPresent(prescriptionDiagnosis)) {
                for (Integer prescDiagnosisId : prescriptionDiagnosis) {
                    Thread.sleep(1500L);

                    Ailment ailment = ailments.getAilments().get(prescDiagnosisId.intValue());

                    wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:prescriptionRegion:0:ctb1")));
                    syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:ctb1")).click();
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")));

                    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(syntagografisiDriver.findElement(By.xpath("/html/body/div[1]/form/div[2]/div[2]/div[1]/table/tbody/tr/td/div/div/table/tbody/tr[2]/td[2]/div/div/iframe")).getAttribute("id")));

                    String xpathOfICD = "/html/body/div/form/div/div/div/div/div[1]/div/div/div/div[1]/div/table/tbody/tr/td[2]/div/div[1]/div/div/div/div[2]/div/div/div/div/div/div/div/div/div/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input";
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathOfICD)));
                    syntagografisiDriver.findElement(By.xpath(xpathOfICD)).sendKeys(ailment.getICDCode());
                    syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'���������')]")).click();
                    ByChained byChained = new ByChained(By.id("t1::db"), By.tagName("table"), By.tagName("tr"));
                    wait.until(ExpectedConditions.visibilityOfElementLocated(byChained));
                    List<WebElement> sessionRows = syntagografisiDriver.findElements(byChained);
                    List<WebElement> sessionData = sessionRows.get(0).findElements(By.tagName("td"));
                    wait.until(ExpectedConditions.visibilityOf(sessionData.get(0).findElement(By.tagName("input"))));
                    sessionData.get(0).findElement(By.tagName("input")).click();
                    syntagografisiDriver.findElement(By.xpath("//button[contains(text(),'���������')]")).click();
                }

                Wait<WebDriver> wait2 = new FluentWait(syntagografisiDriver).withTimeout(5L, TimeUnit.MINUTES).pollingEvery(1L, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

                wait2.until(ExpectedConditions.visibilityOfElementLocated(By.id("pt1:prescriptionRegion:0:itTreat:0:pfl4")));
                WebElement element = syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:itTreat:0:_soc3::content"));

                if (element.getAttribute("title").equals("0")) {
                    Select select = new Select(syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:itTreat:0:_it7::content")));
                    select.selectByValue("0");
                    select = new Select(syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:itTreat:0:_soc4::content")));
                    select.selectByValue("2");
                    select = new Select(syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:itTreat:0:_soc5::content")));
                    select.selectByValue("0");
                    select = new Select(syntagografisiDriver.findElement(By.id("pt1:prescriptionRegion:0:itTreat:0:_soc6::content")));
                    select.selectByValue("0");
                }
            }
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }


    public List<Integer> getPrescriptionDiagnosis() {
        return this.prescriptionDiagnosis;
    }

    public void setPrescriptionDiagnosis(List<Integer> prescriptionDiagnosis) {
        this.prescriptionDiagnosis = prescriptionDiagnosis;
    }

    public Ailments getAilments() {
        return this.ailments;
    }

    public void setAilments(Ailments ailments) {
        this.ailments = ailments;
    }

    public synchronized void startSyntagografisi()
            throws Exception {
        try {
            propertyManager = new PropertiesManager(ExecuteEOPYY.CONF_PROP_FILE);
            debugMode = propertyManager.getProperty("debug.mode");
            if (ValidationUtil.fieldPresent(propertyManager)) {
                boolean success = false;
                WebDriverWait wait = new WebDriverWait(syntagografisiDriver, 10L);
                int numberOfTries = 0;
                while (!success) {
                    try {
                        if (numberOfTries > 4) {
                            break;
                        }
                        wait.until(ExpectedConditions.elementToBeClickable(By.id("pt1:password::content"))).clear();
                        syntagografisiDriver.findElement(By.id("pt1:username::content")).clear();
                        syntagografisiDriver.findElement(By.id("pt1:username::content")).sendKeys(propertyManager.getProperty("username"));
                        syntagografisiDriver.findElement(By.id("pt1:password::content")).clear();
                        byte[] decodedBytes = Base64.decodeBase64(propertyManager.getProperty("password"));
                        syntagografisiDriver.findElement(By.id("pt1:password::content")).sendKeys(new String(decodedBytes));
                        syntagografisiDriver.findElement(By.id("pt1:password::content")).sendKeys(Keys.TAB);
                        success = true;
                    } catch (Exception e) {
                        ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                        success = false;
                        numberOfTries++;
                    }
                }
            }
            if (Boolean.TRUE.toString().equals(debugMode)) {
                ExecuteEOPYY.timerLogger.info("Syntagografisi was initiated");
            }
        } catch (Exception e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
    }

    public void run() {
    }

    public static String replaceCharAt(String s, int pos, char c) {
        return s.substring(0, pos) + c + s.substring(pos + 1);
    }

    public WebDriver getDriver() {
        return this.syntagografisiDriver;
    }

    public void setDriver(WebDriver driver) {
        this.syntagografisiDriver = driver;
    }

    public void tearDown() {
        if (ValidationUtil.fieldPresent(this.syntagografisiDriver)) {
            this.syntagografisiDriver.quit();
            this.syntagografisiDriver.close();
        }
    }

    private boolean isElementPresent(By by) {
        try {
            this.syntagografisiDriver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
        }
        return false;
    }

    public void startRemindTimer(int sec) {
        this.seconds = sec;
        this.timer = new Timer();
        this.timer.schedule(new RemindTask(), this.seconds * 1000);
    }

    public String getAmka() {
        return this.amka;
    }

    public void setAmka(String amka) {
        this.amka = amka;
    }

    public int getDiseaseNumber() {
        return this.diseaseNumber;
    }

    public void setDiseaseNumber(int diseaseNumber) {
        this.diseaseNumber = diseaseNumber;
    }

    public Examinations getExaminations() {
        return this.examinations;
    }

    public void setExaminations(Examinations examinations) {
        this.examinations = examinations;
    }

    public String getReasonOfVisit() {
        return this.reasonOfVisit;
    }

    public void setReasonOfVisit(String reasonOfVisit) {
        this.reasonOfVisit = reasonOfVisit;
    }

    public Integer getSecSyntagografisi() {
        return this.secSyntagografisi;
    }

    public void setSecSyntagografisi(Integer secSyntagografisi) {
        this.secSyntagografisi = secSyntagografisi;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    class RemindTask extends TimerTask {
        RemindTask() {
        }

        public void run() {
            try {
                SwingWorker<Void, Void> worker = new SwingWorker() {
                    protected Void doInBackground()
                            throws Exception {
                        WebElement element = null;
                        for (int i = 0; i <= 20; i++) {
                            try {
                                element = syntagografisiDriver.findElement(By.id("j_id" + i + "::ok"));
                                element.click();
                                if (Boolean.TRUE.toString().equals(Syntagografisi.this.debugMode)) {
                                    ExecuteEOPYY.timerLogger.info("expired timer at " + new Date() + " clicked on j_id" + i + "::ok");
                                }
                            } catch (Exception e) {
                                if (Boolean.TRUE.toString().equals(Syntagografisi.this.debugMode)) {
                                    ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
                                }
                            }
                        }
                        return null;
                    }
                };
                worker.execute();
            } catch (Exception e) {
                ExecuteEOPYY.timerLogger.info(e.getLocalizedMessage());
            } finally {
                timer = new Timer();
                timer.schedule(new RemindTask(), seconds * 1000);
            }
        }
    }
}
