package flow;

import internal.Ailments;
import internal.Examinations;
import org.apache.log4j.PropertyConfigurator;
import utilities.AilmentsUtil;
import utilities.ExamsUtil;
import utilities.PropertiesManager;
import utilities.ValidationUtil;

import javax.swing.*;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;

public class ExecuteEOPYY extends EOPYYForm {

    private static final long serialVersionUID = 6424780913776316864L;
    private static Logger logger = Logger.getLogger(ExecuteEOPYY.class.getName());

    public static Logger diagnosisTimer = Logger.getLogger("data.flow.diagnosis");
    public static Logger timerLogger = Logger.getLogger("data.flow.syntagografisi");

    public static String LOG4J_PROP_FILE = "log4j.properties";
    public static String CONF_PROP_FILE = "configuration.properties";


    public static void main(String[] argv) {
        try {

            PropertiesManager logManager = new PropertiesManager(LOG4J_PROP_FILE);
            PropertyConfigurator.configure(logManager.getProperties());
            PropertiesManager configurationManager = new PropertiesManager(CONF_PROP_FILE);
            String debugMode = configurationManager.getProperty("debug.mode");

            if (Boolean.TRUE.toString().equals(debugMode)) {
                Handler halder = null;
                try {
                    halder = new FileHandler("logs/syntagogafrisi-timer-%g.log", 10000000, 10);
                } catch (Exception ex) {
                    logger.severe(ex.getMessage());
                }
                if (halder != null) {
                    timerLogger.addHandler(halder);
                }
            }

            if (ValidationUtil.fieldPresent(configurationManager)) {
                SwingUtilities.invokeLater(() -> {
                    Examinations exams = ExamsUtil.getExamsAsObject();
                    Ailments ailments = AilmentsUtil.getExamsAsObject();

                    Syntagografisi syntagografisi = new Syntagografisi(
                            configurationManager.getProperty("syntagografisi.value"),
                            configurationManager.getProperty("url"));
                    syntagografisi.setExaminations(exams);
                    syntagografisi.setAilments(ailments);

                    int secSyntagografisi = Integer.valueOf(configurationManager.getProperty("seconds.syntagografisi")).intValue();
                    syntagografisi.setSecSyntagografisi(Integer.valueOf(secSyntagografisi));
                    syntagografisi.startRemindTimer(secSyntagografisi);

                    DisplayDiseases displayDiseases = new DisplayDiseases(exams);

                    JFrame frame = new EOPYYForm(syntagografisi, displayDiseases);
                    frame.setVisible(true);
                });
            }
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }
    }
}