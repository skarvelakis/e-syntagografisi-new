package flow;

import org.apache.commons.codec.binary.Base64;
import utilities.PropertiesManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Logger;

public class Logon
        extends JFrame
        implements ActionListener {
    private static final long serialVersionUID = 1L;
    private JButton bConn;
    private JButton bClose;
    private JTextField UserName;
    private JPasswordField Password;
    private static Logger logger = Logger.getLogger(EOPYYForm.class.getName());

    PropertiesManager configurationManager;

    public Logon() {
        configurationManager = new PropertiesManager(ExecuteEOPYY.CONF_PROP_FILE);
    }

    public void startLogon() {
        setTitle("�������� ���������� ���������");
        this.bConn = new JButton("����������");
        this.bClose = new JButton("��������");
        this.bConn.addActionListener(this);
        this.bClose.addActionListener(this);

        JPanel p = new JPanel();
        p.add(this.bConn);
        p.add(this.bClose);

        JLabel lname = new JLabel("�����");
        JLabel lpassword = new JLabel("�������");

        this.UserName = new JTextField(configurationManager.getProperty("username"), 10);
        this.UserName.setEditable(false);
        byte[] decodedBytes = Base64.decodeBase64(configurationManager.getProperty("password"));
        this.Password = new JPasswordField(new String(decodedBytes));

        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(2, 2));
        p1.add(lname);
        p1.add(this.UserName);
        p1.add(lpassword);
        p1.add(this.Password);

        p1.add(this.UserName);
        p1.add(this.Password);

        getContentPane().add(p1, "North");
        getContentPane().add(p, "Center");

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                dispose();
            }
        });
        setSize(500, 100);
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        int x = (screenSize.width - getWidth()) / 2;
        int y = (screenSize.height - getHeight()) / 2;
        setLocation(x, y);
        setVisible(true);
        setResizable(false);
    }


    public void checkLogin(String userName, String password) {
        try {
            PropertiesManager propertyManager = new PropertiesManager(ExecuteEOPYY.CONF_PROP_FILE);
            File file = new File(ExecuteEOPYY.CONF_PROP_FILE);
            propertyManager.setProperty("username", userName);
            byte[] encodedBytes = Base64.encodeBase64(password.getBytes());
            propertyManager.setProperty("password", new String(encodedBytes));
            file.delete();
            FileOutputStream fileOut;
            fileOut = new FileOutputStream(ExecuteEOPYY.CONF_PROP_FILE);
            propertyManager.getProperties().store(fileOut, null);
            JOptionPane.showMessageDialog(this, "� ������� ���� ��������");
            dispose();
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("��������")) {
            dispose();
        }
        if (e.getActionCommand().equals("����������")) {
            checkLogin(this.UserName.getText(), this.Password.getText());
        }
    }
}
