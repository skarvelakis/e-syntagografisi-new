package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"description", "number", "ICDCode", "flag"})
@XmlRootElement(name = "Ailment")
public class Ailment implements Serializable {
    private static final long serialVersionUID = 3691485242717038066L;
    @XmlElement(name = "description")
    protected String description;
    @XmlElement(name = "ICDCode")
    protected String ICDCode;
    @XmlElement(name = "number")
    protected String number;
    @XmlElement(name = "flag")
    protected String flag;

    public String getFlag() {
        return this.flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getICDCode() {
        return this.ICDCode;
    }

    public void setICDCode(String iCDCode) {
        this.ICDCode = iCDCode;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
