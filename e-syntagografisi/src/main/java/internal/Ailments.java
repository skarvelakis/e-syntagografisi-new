package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"Ailment"})
@XmlRootElement(name = "Ailments")
public class Ailments implements Serializable {
    private static final long serialVersionUID = -6376174850999131252L;
    @XmlElement(name = "Ailment")
    protected List<Ailment> Ailment;

    public List<Ailment> getAilments() {
        if (this.Ailment == null) {
            this.Ailment = new ArrayList();
        }
        return this.Ailment;
    }
}
