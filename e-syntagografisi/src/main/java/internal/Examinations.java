package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"Disease"})
@XmlRootElement(name = "Examinations")
public class Examinations implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "Disease")
    protected List<Disease> Disease;

    public List<Disease> getDiseases() {
        if (this.Disease == null) {
            this.Disease = new ArrayList();
        }
        return this.Disease;
    }
}
