package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"description", "xPath", "xPathNr", "pageNumber"})
@XmlRootElement(name = "Exam")
public class Exam implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "description")
    protected String description;
    @XmlElement(name = "xPath")
    protected String xPath;
    @XmlElement(name = "xPathNr")
    protected String xPathNr;
    @XmlElement(name = "pageNumber")
    protected String pageNumber;

    public String getPageNumber() {
        return this.pageNumber;
    }

    public void setPageNumber(String pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getxPathNr() {
        return this.xPathNr;
    }

    public void setxPathNr(String xPathNr) {
        this.xPathNr = xPathNr;
    }

    public String getDescription() {
        return this.description;
    }

    public String getXPath() {
        return this.xPath;
    }

    public void setXPath(String xPath) {
        this.xPath = xPath;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
