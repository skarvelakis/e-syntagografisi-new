package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"categoryNumber", "description", "examinations"})
@XmlRootElement(name = "Category")
public class Category implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "description")
    protected String description;
    @XmlElement(name = "Exam")
    protected List<Exam> examinations;
    @XmlElement(name = "categoryNumber")
    protected String categoryNumber;

    public String getCategoryNumber() {
        return this.categoryNumber;
    }

    public void setCategoryNumber(String categoryNumber) {
        this.categoryNumber = categoryNumber;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Exam> getExams() {
        if (this.examinations == null) {
            this.examinations = new ArrayList();
        }
        return this.examinations;
    }
}
