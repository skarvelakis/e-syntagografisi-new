package internal;

import javax.xml.bind.annotation.XmlRegistry;
import java.io.Serializable;

@XmlRegistry
public class ObjectFactory implements Serializable {
    private static final long serialVersionUID = 1L;

    public Examinations createExaminations() {
        return new Examinations();
    }

    public Disease createDisease() {
        return new Disease();
    }

    public Category createCategory() {
        return new Category();
    }

    public Ailments createAilments() {
        return new Ailments();
    }

    public Ailment createAilment() {
        return new Ailment();
    }

    public Exam createExam() {
        return new Exam();
    }
}
