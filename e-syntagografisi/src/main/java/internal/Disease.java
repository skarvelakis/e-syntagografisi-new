package internal;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"illness", "number", "Category", "ICDCode"})
@XmlRootElement(name = "Disease")
public class Disease implements Serializable {
    private static final long serialVersionUID = 1L;
    @XmlElement(name = "illness")
    protected String illness;
    @XmlElement(name = "Category")
    protected List<Category> Category;
    @XmlElement(name = "number")
    protected String number;
    @XmlElement(name = "ICDCode")
    protected List<String> ICDCode;

    public List<String> getICDCodeList() {
        if (this.ICDCode == null) {
            this.ICDCode = new ArrayList();
        }
        return this.ICDCode;
    }

    public String getIllness() {
        return this.illness;
    }

    public void setIllness(String illness) {
        this.illness = illness;
    }

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Category> getCategories() {
        if (this.Category == null) {
            this.Category = new ArrayList();
        }
        return this.Category;
    }
}
